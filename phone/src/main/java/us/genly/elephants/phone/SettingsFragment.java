package us.genly.elephants.phone;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import static android.content.Context.MODE_PRIVATE;

/**
 * Loads settings fragment, initializes version.
 *
 * Created by genly on 10/6/2017.
 */

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setVersionPreference();


        // Load the preferences from an XML resource
        addPreferencesFromResource(us.genly.elephants.phone.R.xml.preferences);

        Preference versionPref = findPreference("version");
        versionPref.setSummary(getAppVersion());
    }

    private void setVersionPreference() {
        final Context context = getContext();
        SharedPreferences.Editor prefs = context.getSharedPreferences(
                PreferenceManager.getDefaultSharedPreferencesName(context), MODE_PRIVATE).edit();
        prefs.putString("version", getAppVersion());
        prefs.commit();
    }

    public String getAppVersion() {
        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "Can't read version";
        }
    }
 }