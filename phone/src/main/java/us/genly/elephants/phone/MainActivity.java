package us.genly.elephants.phone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Timer;
import java.util.TimerTask;

import us.genly.elephants.common.ElephantGLSurfaceView;
import us.genly.elephants.Model;

public class MainActivity extends AppCompatActivity {
    private static final String STATE_SHOW_THOUGHTS = "show_thoughts";
    private static final String STATE_VOLUME = "volume";
    private ElephantGLSurfaceView _glElephantView;
    private Timer _timer;
    private SoundPool _soundPool;
    private int _soundID;
    private static final String TAG = "Elephants";
    private float _volume = VolumePreference.DEFAULT_VALUE;
    private boolean _showThoughts;
    private Model _model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(us.genly.elephants.phone.R.layout.main_activity);
        Toolbar toolbar = findViewById(us.genly.elephants.phone.R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            _volume = savedInstanceState.getInt(STATE_VOLUME);
            _showThoughts = savedInstanceState.getBoolean(STATE_SHOW_THOUGHTS);
        } else
            loadPreferences();
        Log.d(TAG, "onCreate volume=" + _volume + " showThoughts=" + _showThoughts);
        _model = createModel();
        _model.setShowThoughts(_showThoughts);

        _glElephantView = findViewById(us.genly.elephants.phone.R.id.surface);
        _glElephantView.setModel(_model);

        createSoundPlayer(_glElephantView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(us.genly.elephants.phone.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == us.genly.elephants.phone.R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putFloat(STATE_VOLUME, _volume);
        savedInstanceState.putBoolean(STATE_SHOW_THOUGHTS, _showThoughts);

        Log.d(TAG, "onSaveInstanceState volume=" + _volume + " showThoughts=" + _showThoughts);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        savePreferences();
    }

    private void loadPreferences() {
        SharedPreferences prefs = getSharedPreferences(
                PreferenceManager.getDefaultSharedPreferencesName(this), MODE_PRIVATE);
        _showThoughts = prefs.getBoolean(STATE_SHOW_THOUGHTS, false);
        _volume = prefs.getFloat(STATE_VOLUME, VolumePreference.DEFAULT_VALUE);
        if (_model != null)
            _model.setShowThoughts(_showThoughts);
        Log.d(TAG, "loadPreferences volume=" + _volume + " _showThoughts=" + _showThoughts);
    }

    private void savePreferences() {
        SharedPreferences.Editor prefs = getSharedPreferences(
                PreferenceManager.getDefaultSharedPreferencesName(this), MODE_PRIVATE).edit();
        prefs.putFloat(STATE_VOLUME, _volume);
        prefs.putBoolean(STATE_SHOW_THOUGHTS, _showThoughts);
        prefs.commit();
        Log.d(TAG, "savePreferences volume=" + _volume + " showThoughts=" + _showThoughts);
    }

    @NonNull
    private Model createModel() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        Model model = new Model(displayMetrics.widthPixels, displayMetrics.heightPixels);
        model.setScale(4);
        return model;
    }

    private void createSoundPlayer(ElephantGLSurfaceView glElephantView) {
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        _soundPool = new SoundPool.Builder().setMaxStreams(1).build();
        _soundID = _soundPool.load(this, us.genly.elephants.phone.R.raw.elephant, 1);

        glElephantView.setElephantSound(new ElephantGLSurfaceView.IElephantSound() {
            @Override
            public void play() {
                _soundPool.play(_soundID, _volume, _volume, 1, 0, 1);
            }
        });
    }

    /**
     * Stop the animation timer.
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "pause");
        _timer.cancel();
    }

    /**
     * Start the animation timer.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "resume");
        _timer = new Timer(true);
        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                _glElephantView.next();
            }
        }, 100, 100);
        loadPreferences();
    }
}
