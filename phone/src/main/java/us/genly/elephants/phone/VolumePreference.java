package us.genly.elephants.phone;

/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class VolumePreference extends Preference
        implements OnSeekBarChangeListener {

    public static final int SEEK_BAR_MAX = 100;
    public static final float DEFAULT_VALUE = .25f;
    private float _volume;

    public VolumePreference(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

//        TypedArray a = context.obtainStyledAttributes(
//                attrs, R.styleable.VolumePreference, defStyleAttr, defStyleRes);
//        int knobColor = a.getColor(R.styleable.VolumePreference_knobColor, 0);
//        a.recycle();

        setDefaultValue(DEFAULT_VALUE);
        setLayoutResource(R.layout.volume_preference);
    }

    public VolumePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public VolumePreference(Context context, AttributeSet attrs) {
        this(context, attrs, R.style.SeekBarPreferenceStyle);
    }

    public VolumePreference(Context context) {
        this(context, null);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        SeekBar seekBar = view.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setMax(SEEK_BAR_MAX);
        seekBar.setProgress((int) (_volume * SEEK_BAR_MAX));
        seekBar.setEnabled(isEnabled());
        updateVolumeDownIcon();
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setVolume(restoreValue ? getPersistedFloat(DEFAULT_VALUE)
                : (Float) defaultValue);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getFloat(index, DEFAULT_VALUE);
    }

    public void setVolume(float progress) {
        setVolume(progress, true);
    }

    private void setVolume(float volume, boolean notifyChanged) {
        if (volume > 1f) {
            volume = 1f;
        }
        if (volume < 0f) {
            volume = 0f;
        }
        if (volume != _volume) {
            _volume = volume;
            persistFloat(volume);
            if (notifyChanged) {
                notifyChanged();
            }
        }
    }

    /**
     * Persist the seekBar's progress value if callChangeListener
     * returns true, otherwise set the seekBar's progress to the stored value
     */
    void syncProgress(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        float volume = progress / (float) SEEK_BAR_MAX;
        if (volume != _volume) {
            if (callChangeListener(volume)) {
                setVolume(volume, false);
            } else {
                seekBar.setProgress((int) (_volume * SEEK_BAR_MAX));
            }
        }
        updateVolumeDownIcon();
    }

    @Override
    public void onProgressChanged(
            SeekBar seekBar, int progress, boolean fromUser) {
        syncProgress(seekBar);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        /*
         * Suppose a client uses this preference type without persisting. We
         * must save the instance state so it is able to, for example, survive
         * orientation changes.
         */

        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent()) {
            // No need to save instance state since it's persistent
            return superState;
        }

        // Save the instance state
        final SavedState myState = new SavedState(superState);
        myState.progress = _volume;
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!state.getClass().equals(SavedState.class)) {
            // Didn't save state for us in onSaveInstanceState
            super.onRestoreInstanceState(state);
            return;
        }

        // Restore the instance state
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());
        _volume = myState.progress;
        notifyChanged();
    }

    /**
     * SavedState, a subclass of {@link BaseSavedState}, will store the state
     * of MyPreference, a subclass of Preference.
     * <p>
     * It is important to always call through to super methods.
     */
    private static class SavedState extends BaseSavedState {
        float progress;
        float max;

        public SavedState(Parcel source) {
            super(source);

            // Restore the click counter
            progress = source.readInt();
            max = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);

            // Save the click counter
            dest.writeFloat(progress);
            dest.writeFloat(max);
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        return super.onCreateView(parent);
    }

    private void updateVolumeDownIcon() {
        if (_volume == 0)
            setIcon(R.drawable.ic_volume_off_black_24dp);
        else
            setIcon(R.drawable.ic_volume_down_black_24dp);
    }
}
