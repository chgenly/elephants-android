package us.genly.elephants.midlet;

import java.io.IOException;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import us.genly.elephants.actors.Elephant;
import us.genly.elephants.actors.Room;
import us.genly.elephants.graphics.Debug;

public class ElephantMidlet extends MIDlet implements HelpListener {
	ElephantCanvas elephantCanvas;
	Display display;
	
	protected void startApp() throws MIDletStateChangeException {
		display = Display.getDisplay(this);
		Debug.display = display;
		elephantCanvas = new ElephantCanvas(this);
		elephantCanvas.addHelpListener(this);
		display.setCurrent(elephantCanvas);
        Elephant.showThoughts(true);
        int w = elephantCanvas.getWidth()*2;
        int h = elephantCanvas.getHeight()*2;
        Room room = new Room(w, h);
        elephantCanvas.setRoom(room);
        
		Image weeds=null;
		try {
			weeds = Image.createImage("/weeds.png");
			elephantCanvas.init(weeds);
		} catch (IOException e) {
			Alert a = new Alert(e.toString());
			display.setCurrent(a);
			return;
		}
        for(int i=0; i<3; ++i) {
            Elephant e = new Elephant(elephantCanvas.room);
            e.setShock(i%2 == 0);
            elephantCanvas.room.add(e);
        }

		elephantCanvas.start();
	}

	protected void pauseApp() {
		elephantCanvas.stop();
	}

	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		elephantCanvas.stop();
	}
	
	public void helpRequested() {
		Alert fm = new Alert("Help");
		fm.setString("1 add peanut\n" +
				"3 add mouse\n" +
				"4 add elephant\n" +
				"5 toggle thoughts\n" +
				"6 remove elephant");
		display.setCurrent(fm);
	}
}
