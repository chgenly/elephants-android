package us.genly.elephants.midlet;
import java.util.Enumeration;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;

import us.genly.elephants.actors.Elephant;
import us.genly.elephants.actors.Mouse;
import us.genly.elephants.actors.Peanut;
import us.genly.elephants.actors.Room;
import us.genly.elephants.graphics.EColors;
import us.genly.elephants.graphics.Random;

public class ElephantCanvas extends Canvas implements CommandListener, Runnable {
	private static final long serialVersionUID = 4456207048643121947L;
	int w, h;
    Thread thread;
    Image osi;
    Graphics osg;
    MidletEGraphics eosg;
    Image outside;
    public Room room;
    MIDlet midlet;
    java.util.Random rand = new java.util.Random(System.currentTimeMillis());
	private HelpListener helpListener;

    public ElephantCanvas(MIDlet midlet) {
    	this.midlet = midlet;
    	addCommand(new Command("Help", Command.SCREEN, 1));
    	addCommand(new Command("Exit", Command.EXIT, 1));
    	setCommandListener(this);    	
	}
    
    public void addHelpListener(HelpListener helpListener) {
    	this.helpListener = helpListener;
    }
    
    public void init(Image outside)
    {
        this.outside = outside;
    }
    
    public void setRoom(Room room) {
    	this.room = room;
    }
    
    public void start()
    {
        w = getWidth();
        h = getHeight();
        osi = Image.createImage(w, h);
        osg = osi.getGraphics();
        eosg = new MidletEGraphics(osg);
        thread = new Thread(this);
        thread.start();
    }

    public void stop()
    {
        thread = null;
    }

    public void paint(Graphics g) {
        g.drawImage(osi, 0, 0, Graphics.TOP|Graphics.LEFT);
    }

    public void run()
    {
        try {
            while(thread != null) {
                osg.setColor(EColors.black.r, EColors.black.g, EColors.black.b);
                osg.fillRect(0, 0, w, h);
                osg.drawImage(outside, 0, 0, Graphics.TOP|Graphics.LEFT);
                room.next();
                room.paint(eosg);
                repaint();
                Thread.sleep(100);                
            }
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(10);
        }
    }

    public void feedPeanut(int x, int y)
    {
        room.add(new Peanut(x, y));
    }

    public void keyPressed(int key) {
    	switch(key) {
    	case KEY_NUM1:
			feedPeanut(Random.nextInt(room.w), Random.nextInt(room.h));
    		break;
    	case KEY_NUM3:
            room.add(new Mouse(room, rand.nextInt()));
    		break;
		case KEY_NUM4:
            Elephant e = new Elephant(room);
            room.add(e);
			break;
		case KEY_NUM5:
	        Elephant.showThoughts(!Elephant.getThoughts());
			break;
		case KEY_NUM6:
			for(Enumeration en=room.roomObjects(); en.hasMoreElements(); ) {
				Object o = en.nextElement();
				if (o instanceof Elephant) {
					Elephant elephant = (Elephant) o;
					room.removeActor(elephant);
					break;
				}
			}
			break;
		case KEY_NUM0:
			thread = null;
			midlet.notifyDestroyed();
    	}
    }
    
	public void commandAction(Command cmd, Displayable display) {
		if (cmd.getLabel().equals("Help")) {
			helpListener.helpRequested();
		} else if (cmd.getLabel().equals("Exit")) {
			thread = null;
			midlet.notifyDestroyed();
		}		
	}	
}
