package us.genly.elephants.midlet;

import javax.microedition.lcdui.Graphics;

import us.genly.elephants.graphics.EColor;
import us.genly.elephants.graphics.EGraphics;

public class MidletEGraphics implements EGraphics{
	Graphics g;
	
	public MidletEGraphics(Graphics g) {
		this.g = g;
	}

	public void drawOval(int x, int y, int w, int h) {
		g.drawArc(x/2, y/2, w/2, h/2, 0, 360);
	}

	public void fillOval(int x, int y, int w, int h) {
		g.fillArc(x/2, y/2, w/2, h/2, 0, 360);
	}

	public void fillRect(int x, int y, int w, int h) {
		g.fillRect(x/2, y/2, w/2, h/2);
	}

	public void drawLine(int x0, int y0, int x1, int y1) {
		g.drawLine(x0/2, y0/2, x1/2, y1/2);
	}

	public void setColor(EColor color) {
		g.setColor(color.r, color.g, color.b);		
	}

}
