package us.genly.elephants.common;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

/**
 * Created by genly on 10/17/2017.
 */

public class LabelPreference extends EditTextPreference {
    public LabelPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setSelectable(false);
    }

    public LabelPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setSelectable(false);
    }

    public LabelPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSelectable(false);
    }

    public LabelPreference(Context context) {
        super(context);
        setSelectable(false);
    }
}
