package us.genly.elephants.common;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import us.genly.elephants.Model;
import us.genly.elephants.common.ElephantGLRenderer;

/**
 * An open gl es surface to render elephants on.  Also
 * handles short click to create a peanut, and long click
 * to create a mouse.
 *
 * Created by genly on 9/23/2017.
 */
public class ElephantGLSurfaceView extends GLSurfaceView {
    private static final String TAG = "Elephant";
    private int x, y;
    private final ElephantGLRenderer mRenderer;
    public interface IElephantSound {
        void play();
    }
    private IElephantSound _sound = new IElephantSound() {
        @Override
        public void play() {
        }
    };

    public ElephantGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLConfigChooser(new ElephantConfigChooser());

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new ElephantGLRenderer();

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick");
                createPeanut(x, y);
            }
        });

        setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d(TAG, "onLongClick");
                createMouse(x, y);
                return true;
            }
        });

    }

    public ElephantGLSurfaceView(Context context) {
        this(context, null);
    }

    public void setElephantSound(IElephantSound sound) {
        _sound = sound;
    }

    public void setModel(Model model) {
        mRenderer.setModel(model);
    }


    /**
     * Capture the x and y for use in the OnClick and OnLongClick listeners.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        Log.d(TAG, "onTouch");
        x = (int) event.getX();
        y = (int) event.getY();
        return true;
    }

    public void next() {
        mRenderer.next();
        requestRender();
    }

    public void createPeanut(int x, int y) {
        mRenderer.createPeanut(x, y);
    }

    public void createMouse(int x, int y) {
        _sound.play();
        mRenderer.createMouse(x, y);
    }

    private static class ElephantConfigChooser implements GLSurfaceView.EGLConfigChooser {
        @Override
        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            int attributes[];

            if (android.os.Build.MODEL.contains("x86")) {
                // Emulator crashes when using anti-aliasing.
                attributes = new int[] {
                        EGL10.EGL_NONE
                };
            } else {
                attributes = new int[] {
                        EGL10.EGL_SAMPLES, 4,  // This is for 4x MSAA.
                        EGL10.EGL_NONE
                };
            }
            EGLConfig[] configs = new EGLConfig[1];
            int[] configCounts = new int[1];
            egl.eglChooseConfig(display, attributes, configs, 1, configCounts);
            if (configCounts[0] != 0) {
                return configs[0];
            } else {
                // Failed! Error handling.
                return null;
            }
        }
    }
}
