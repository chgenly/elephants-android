package us.genly.elephants.common;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import us.genly.elephants.Model;
import us.genly.elephants.graphics.EColor;
import us.genly.elephants.graphics.EGraphics;

/**
 * Performs the rendering of elephants using OPEN GL ES
 *
 * Created by genly on 9/23/2017.
 */
class ElephantGLRenderer implements GLSurfaceView.Renderer {
    private int mPositionHandle;
    private int mColorHandle;
    private EGraphics g;
    private Model _model;

    private int mProgram;
    private FloatBuffer vertexBuffer;


    private static final int MAX_POINTS = 30;
    private static final int COORDINATES_PER_VERTEX = 3;
    private static final int vertexStride = COORDINATES_PER_VERTEX * 4; // 4 bytes per vertex
    private AtomicBoolean _gotoNext = new AtomicBoolean();

    public ElephantGLRenderer() {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                MAX_POINTS * COORDINATES_PER_VERTEX * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glUseProgram(mProgram);

        g = new EGraphics() {
            @Override
            public void drawOval(float x, float y, float w, float h) {
                oval(x, y, w, h, GLES20.GL_LINE_LOOP);
            }

            @Override
            public void fillOval(float x, float y, float w, float h) {
                oval(x, y, w, h, GLES20.GL_TRIANGLE_FAN);
            }

            private void oval(float x, float y, float w, float h, int mode) {
                float rx = w / 2;
                float ry = h / 2;
                float x0 = x + rx;
                float y0 = y + ry;
                vertexBuffer.position(0);
                for (int i = 0; i < MAX_POINTS; ++i) {
                    float theta = 6.28f / MAX_POINTS * i;
                    float xp = (float) (x0 + rx * Math.cos(theta));
                    float yp = (float) (y0 + ry * Math.sin(theta));
                    vertexBuffer.put(xp);
                    vertexBuffer.put(yp);
                    vertexBuffer.put(0);
                }
                vertexBuffer.position(0);

                GLES20.glEnableVertexAttribArray(mPositionHandle);
                GLES20.glVertexAttribPointer(mPositionHandle, COORDINATES_PER_VERTEX,
                        GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);
                GLES20.glDrawArrays(mode, 0, MAX_POINTS);
                GLES20.glDisableVertexAttribArray(mPositionHandle);
            }

            @Override
            public void fillRect(float x, float y, float w, float h) {
                // add the coordinates to the FloatBuffer
                vertexBuffer.position(0);
                vertexBuffer.put(x);
                vertexBuffer.put(y);
                vertexBuffer.put(0);
                vertexBuffer.put(x + w);
                vertexBuffer.put(y);
                vertexBuffer.put(0);
                vertexBuffer.put(x + w);
                vertexBuffer.put(y + h);
                vertexBuffer.put(0);
                vertexBuffer.put(x);
                vertexBuffer.put(y + h);
                vertexBuffer.put(0);
                vertexBuffer.position(0);
                GLES20.glEnableVertexAttribArray(mPositionHandle);
                GLES20.glVertexAttribPointer(mPositionHandle, COORDINATES_PER_VERTEX,
                        GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);
                GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
                GLES20.glDisableVertexAttribArray(mPositionHandle);
            }

            @Override
            public void drawLine(float x0, float y0, float x1, float y1) {
                vertexBuffer.position(0);
                vertexBuffer.put(x0);
                vertexBuffer.put(y0);
                vertexBuffer.put(0);
                vertexBuffer.put(x1);
                vertexBuffer.put(y1);
                vertexBuffer.put(0);
                vertexBuffer.position(0);
                GLES20.glEnableVertexAttribArray(mPositionHandle);
                GLES20.glVertexAttribPointer(mPositionHandle, COORDINATES_PER_VERTEX,
                        GLES20.GL_FLOAT, false, vertexStride, vertexBuffer);
                GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, 2);
                GLES20.glDisableVertexAttribArray(mPositionHandle);
            }

            @Override
            public void setColor(EColor color) {
                float[] array = new float[]{color.r / 255f, color.g / 255f, color.b / 255f, 1};
                GLES20.glUniform4fv(mColorHandle, 1, array, 0);
            }

            @Override
            public void setScale(float v) {

            }
        };
    }

    private final float[] mMVPMatrix = new float[16];

    public void onDrawFrame(GL10 unused) {
        // Redraw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        try {
            String vertexShaderCode = "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";
            int vertexShader = ElephantGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
                    vertexShaderCode);
            String fragmentShaderCode = "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";
            int fragmentShader = ElephantGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
                    fragmentShaderCode);
            mProgram = GLES20.glCreateProgram();
            GLES20.glAttachShader(mProgram, vertexShader);
            GLES20.glAttachShader(mProgram, fragmentShader);
            GLES20.glLinkProgram(mProgram);
            GLES20.glUseProgram(mProgram);

            mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
            mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

            Matrix.orthoM(mMVPMatrix, 0, 0, _model.getWidth(), _model.getHeight(), 0, -1, 1);
            int mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

            _model.paint(g);
            if (_gotoNext.getAndSet(false)) {
                _model.next();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setModel(Model model) {
        _model = model;
    }

    public void next() {
        _gotoNext.set(true);
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    public static int loadShader(int type, String shaderCode) {

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    public void createPeanut(int x, int y) {
        _model.createPeanut(x, y);
    }

    public void createMouse(int x, int y) {
        _model.createMouse(x, y);
    }
}
