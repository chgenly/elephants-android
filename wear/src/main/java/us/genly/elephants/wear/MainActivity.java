package us.genly.elephants.wear;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wear.widget.drawer.WearableDrawerLayout;
import android.support.wear.widget.drawer.WearableDrawerLayout.DrawerStateCallback;
import android.support.wear.widget.drawer.WearableDrawerView;
import android.support.wearable.activity.WearableActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import us.genly.elephants.common.ElephantGLSurfaceView;
import us.genly.elephants.Model;

/**
 * Main activity is to show a GLSurface view animating the elephants and managing
 * settings.
 * <p>
 * 9/26/2017
 */
public class MainActivity extends WearableActivity {
    private static final String STATE_SHOW_THOUGHTS = "showThoughts";
    private static final String STATE_VOLUME = "volume";
    public static final float DEFAULT_VOLUME = .125f;
    private ElephantGLSurfaceView _glElephantView;
    private Timer _timer;
    private SoundPool _soundPool;
    private int _soundID;
    private static final String TAG = "Elephants";
    private float _volume;
    private WearableDrawerView _wearableActionDrawer;
    private boolean _showThoughts;
    private Switch _showThoughtsSwitch;
    private Model _model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            _volume = savedInstanceState.getInt(STATE_VOLUME);
            _showThoughts = savedInstanceState.getBoolean(STATE_SHOW_THOUGHTS);
        } else
            loadPreferences();
        Log.d(TAG, "onCreate volume="+_volume+" showThoughts="+_showThoughts);
        _model = createModel();
        _model.setShowThoughts(_showThoughts);

        setContentView(R.layout.main_activity);
        final WearableDrawerLayout l = findViewById(R.id.drawer_layout);
        _glElephantView  = findViewById(R.id.surface);
        _glElephantView.setModel(_model);

        _showThoughtsSwitch = findViewById(R.id.switch1);

        handleSettings(_model);

        _wearableActionDrawer = handleWearableDrawer(l);

        createSoundPlayer(_glElephantView);

        // Let the user know there is a settings window, but don't let it stay in the peek state.
        _wearableActionDrawer.getController().peekDrawer();
        _wearableActionDrawer.getController().closeDrawer();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putFloat(STATE_VOLUME, _volume);
        savedInstanceState.putBoolean(STATE_SHOW_THOUGHTS, _showThoughts);

        Log.d(TAG, "onSaveInstanceState volume=" + _volume + " showThoughts=" + _showThoughts);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        savePreferences();
    }

    private void loadPreferences() {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        _showThoughts = prefs.getBoolean(STATE_SHOW_THOUGHTS, false);
        _volume = prefs.getFloat(STATE_VOLUME, DEFAULT_VOLUME);
        Log.d(TAG, "loadPreferences volume=" + _volume + " showThoughts=" + _showThoughts);
    }

    private void savePreferences() {
        SharedPreferences.Editor prefs = getPreferences(MODE_PRIVATE).edit();
        prefs.putFloat(STATE_VOLUME, _volume);
        prefs.putBoolean(STATE_SHOW_THOUGHTS, _showThoughts);
        prefs.commit();
        Log.d(TAG, "savePreferences volume=" + _volume + " showThoughts=" + _showThoughts);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getRepeatCount() == 0) {
            if (keyCode == KeyEvent.KEYCODE_STEM_1) {
                Log.d(TAG, "onKeyDown: button 1");
                _model.createPeanut();
                return true;
            } else if (keyCode == KeyEvent.KEYCODE_STEM_2) {
                Log.d(TAG, "onKeyDown: button 2");
                _model.createMouse();
                playElephantSound();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @NonNull
    private Model createModel() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        return new Model(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private void handleSettings(final Model _model) {
        _showThoughtsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton button, boolean value) {
                _showThoughts = value;
                _model.setShowThoughts(_showThoughts);
            }
        });
        final ImageButton volumeButton = findViewById(R.id.volume_button);
        updateVolumeButtonIcon();
        volumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "volume pressed");
                Intent intent = new Intent(MainActivity.this, VolumeActivity.class);
                intent.putExtra(VolumeActivity.EXTRA_VOLUME, _volume);
                startActivityForResult(intent, VolumeActivity.SET_VOLUME);
            }
        });
        TextView versionView = findViewById(R.id.version);
        versionView.setText(getAppVersion());
    }

    private void updateVolumeButtonIcon() {
        ImageView volumeDownImageView = findViewById(R.id.volume_button);
        if (_volume == 0f)
            volumeDownImageView.setImageResource(R.drawable.ic_volume_off_black_24dp);
        else
            volumeDownImageView.setImageResource(R.drawable.ic_volume_up_black_24dp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VolumeActivity.SET_VOLUME && data != null) {
            _volume = data.getFloatExtra(VolumeActivity.EXTRA_VOLUME, 1);
            updateVolumeButtonIcon();
            Log.d(TAG, "volume received "+_volume);
        }
        _wearableActionDrawer.getController().closeDrawer();
    }

    private WearableDrawerView handleWearableDrawer(WearableDrawerLayout l) {
        final WearableDrawerView mWearableActionDrawer =
                findViewById(R.id.drawer_view);
        l.setDrawerStateCallback(new DrawerStateCallback() {
            public void onDrawerOpened(WearableDrawerLayout layout, WearableDrawerView drawerView) {
                Log.d(TAG, "onDrawerOpened _showThoughts="+_showThoughts);
                _showThoughtsSwitch.setChecked(_showThoughts);
            }

            public void onDrawerClosed(WearableDrawerLayout layout, WearableDrawerView drawerView) {
            }

            public void onDrawerStateChanged(WearableDrawerLayout layout, @WearableDrawerView.DrawerState int newState) {
                // When the settings drawer closes to a peek state, make it close all the way to
                // free up the screen space.
                if (newState == WearableDrawerView.STATE_IDLE && mWearableActionDrawer.getY() != 0)
                    mWearableActionDrawer.getController().closeDrawer();

                // If the drawer has fully opened, tell it again to open.  Without this, the drawer
                // visually never fully opens.  Seems to be an interaction with GLSurfaceView.
                if (newState == WearableDrawerView.STATE_IDLE && mWearableActionDrawer.getY() == 0)
                    mWearableActionDrawer.getController().openDrawer();
            }
        });
        return mWearableActionDrawer;
    }

    private void createSoundPlayer(ElephantGLSurfaceView glElephantView) {
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        _soundPool = new SoundPool.Builder().setMaxStreams(1).build();
        _soundID = _soundPool.load(this, R.raw.elephant, 1);

        glElephantView.setElephantSound(new ElephantGLSurfaceView.IElephantSound() {
            @Override
            public void play() {
                playElephantSound();
            }
        });
    }

    private void playElephantSound() {
        _soundPool.play(_soundID, _volume, _volume, 1, 0, 1);
    }

    /**
     * Stop the animation timer.
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "pause");
        _timer.cancel();
    }

    /**
     * Start the animation timer.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "resume");
        _timer = new Timer(true);
        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                _glElephantView.next();
            }
        }, 100, 100);
    }

    private String getAppVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "Can't read version";
        }
    }
}
