package us.genly.elephants.wear;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;

import us.genly.elephants.wear.R;

/**
 * Shows volume seek bar.  Initial volume sent as EXTRA_VOLUME.
 * Final volume returned as EXTRA_VOLUME.
 *
 * Created by genly on 10/3/2017.
 */

public class VolumeActivity extends WearableActivity {
    private static final String TAG = "Elephants";
    public static final int SET_VOLUME = 1;
    public static final String EXTRA_VOLUME ="volume";
    private SeekBar _seekBar;
    private float _volume;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.volume);
        _volume = getIntent().getFloatExtra(EXTRA_VOLUME, 1);
        _seekBar = findViewById(R.id.seekBar);
        _seekBar.setProgress((int) (_volume*_seekBar.getMax()));
        _seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                _volume = (float)seekBar.getProgress() / seekBar.getMax();
                updateIcon();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                _volume = (float)seekBar.getProgress() / seekBar.getMax();
                Log.d(TAG, "seek bar volume="+_volume);
                Intent result = new Intent();
                result.putExtra(EXTRA_VOLUME, _volume);
                setResult(Activity.RESULT_OK, result);
            }
        });
        updateIcon();
        Log.d(TAG, "onCreate");
    }

    private void updateIcon() {
        ImageView volumeDownImageView = findViewById(R.id.volumeDownImageView);
        if (_volume == 0f)
            volumeDownImageView.setImageResource(R.drawable.ic_volume_off_white_24dp);
        else
            volumeDownImageView.setImageResource(R.drawable.ic_volume_up_white_24dp);
    }
}
